# Cypress Linux WiFi Driver Release (FMAC) [2023-05-23]
- v5.15.58-2023_0523
- https://community.infineon.com/t5/Wi-Fi-Bluetooth-for-Linux/Cypress-Linux-WiFi-Driver-Release-FMAC-2023-05-23/td-p/445808

# Cypress Linux WiFi Driver Release (FMAC) [2023-02-22]
- v5.15.58-2023_0222
- https://community.infineon.com/t5/Wi-Fi-Bluetooth-for-Linux/Cypress-Linux-WiFi-Driver-Release-FMAC-2023-02-22/m-p/400429
